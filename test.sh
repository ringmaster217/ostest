#!/bin/bash

function exittrap {
  echo "exit trap"
}

function errtrap {
  echo "err trap"
}

function termtrap {
  echo "term trap"
}

function inttrap {
  echo "int trap"
}

trap exittrap EXIT
trap errtrap ERR
trap termtrap SIGTERM
trap inttrap SIGINT

while :
do
  sleep 1
  echo "sleeping..."
done
